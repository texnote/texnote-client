'use strict';

describe('Directive: scribe', function () {

  // load the directive's module
  beforeEach(module('texnoteApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<scribe></scribe>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the scribe directive');
  }));
});
