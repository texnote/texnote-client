TeXnote Client
==============

* [MathJax TeX and LaTeX support](http://docs.mathjax.org/en/latest/tex.html)
* [MathJax Processing Model](http://docs.mathjax.org/en/latest/model.html)
* [Guardian - Scribe](https://github.com/guardian/scribe)
* [TogetherJS](https://togetherjs.com/)
* [ShareJS](http://sharejs.org/)
* [Etherpad](http://etherpad.org/)
* [Operational Transformation](http://en.wikipedia.org/wiki/Operational_transformation)

## Home

Explore some solutions of TeXnote:

* Send small mathematics notes
* Collaborate
* Teach

## About

Basic information on what TeXnote is and what it isn't.
Provide some contact details.
