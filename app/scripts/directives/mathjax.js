'use strict';

/**
 * @ngdoc directive
 * @name texnoteApp.directive:mathjax
 * @description
 * # Two MathJax attribute directives are defined here:
 ^ #   mathjax-bind
 ^ #
 ^ #
 ^ #
 ^ #
 ^ #
 ^ #
 ^ #  mathjax-dynamic-bind
 ^ #
 ^ #
 ^ #
 ^ #
 ^ #
 ^ # Configuration of Mathjax is performed first.
 ^ # We load only the TeX preprocessor and render to HTML and CSS.
 ^ # The usual AMS math and symbols are also loaded.
 */
MathJax.Hub.Config({
  jax: ["input/TeX","output/HTML-CSS", "output/CommonHTML"],
  extensions: ["tex2jax.js","MathMenu.js","MathZoom.js", "CHTML-preview.js"],
  TeX: {
    extensions: ["AMSmath.js","AMSsymbols.js","noErrors.js","noUndefined.js"]
  },
  skipStartupTypeset: true,
  messageStyle: "none",
  "HTML-CSS": {
    showMathMenu: false
  }
});
MathJax.Hub.Configured();

angular.module('texnoteApp')
.directive("mathjaxBind", function() {
  return {
    restrict: "A",
    scope:{
      tex: "@mathjaxBind"
    },
    controller: function($scope, $element, $attrs) {
      $scope.$watch('tex', function(value) {
        var script = angular.element('<script type="math/tex">')
                            .html(value == undefined ? "" : value);
        $element.html("");
        $element.append(script);
        MathJax.Hub.Queue(["Reprocess", MathJax.Hub, $element[0]]);
      });
    }
  };
})
.directive('mathjaxDynamicBind', function($compile) {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      tex: '@mathjaxDynamicBind'
    },
    link: function(scope, element, attrs) {
      scope.$watch('tex', function(html) {
        html = html.replace(/\$\$([^$]+)\$\$/g, '<span class="tex-inline" mathjax-bind="$1"></span>');
        html = html.replace(/\$([^$]+)\$/g, '<div class="tex-block" mathjax-bind="$1"></div>');
        element.html(html);
        $compile(element.contents())(scope);
      });
    }
  };
});
