'use strict';

/**
 * @ngdoc directive
 * @name texnoteApp.directive:scribe
 * @description
 * # scribe
 */
angular.module('texnoteApp')
  .directive('scribe', function(Scribe) {
    return {
      template: '<div class="scribe"></div>',
      restrict: 'E',
      replace: true,
      link: function postLink(scope, element, attrs) {
        scope.scribe = new Scribe(element[0]);
      }
    };
  });
