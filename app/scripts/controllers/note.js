'use strict';

/**
 * @ngdoc function
 * @name texnoteApp.controller:NoteCtrl
 * @description
 * # NoteCtrl
 * Controller of the texnoteApp
 */
angular.module('texnoteApp')
  .controller('NoteCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
