'use strict';

/**
 * @ngdoc function
 * @name texnoteApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the texnoteApp
 */
angular.module('texnoteApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
