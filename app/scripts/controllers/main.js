'use strict';

/**
 * @ngdoc function
 * @name texnoteApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the texnoteApp
 */
angular.module('texnoteApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
