'use strict';

/**
 * @ngdoc overview
 * @name texnoteApp
 * @description
 * # texnoteApp
 *
 * Main module of the application.
 */
angular
  .module('texnoteApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'scribe'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/note', {
        templateUrl: 'views/note.html',
        controller: 'NoteCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
